package com.tyler.footscore.ConfrontationPackage;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.tyler.footscore.R;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ConfrontationResActivity extends AppCompatActivity {

    private String url = "";
    private String linkFinalVideo = "";
    private ConfrontationResAdapter adapter;
    private WebView ytWebView;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.confrontation_res_layout);
        ListView listview = findViewById(R.id.confrontatio_res_list_view);
        ytWebView = findViewById(R.id.ytVideo);

        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        if (b != null) {
            url = (String) b.get("keyUrl");
        }


        adapter = new ConfrontationResAdapter(this);

        ConfrontationResItemAsync confrRes = new ConfrontationResItemAsync(ConfrontationResActivity.this, this, adapter);
        Thread thread = new Thread() {
            public void run() {
                String[] tab = new String[1];
                tab[0] = url;
                JSONObject JsObjUrl = confrRes.doInBackground(tab);
                confrRes.onPostExecute(JsObjUrl);

                runOnUiThread(() -> {
                    displayHighlightVideo();
                    listview.setAdapter(adapter);
                });
            }
        };
        thread.start();
        // disable scroll on touch
        ytWebView.setOnTouchListener((v, event) -> {
            if (ytWebView.getUrl().contains("youtube")) {
                return false;
            } else {
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        });
        ytWebView.reload();

    }

    private void displayHighlightVideo() {
        runOnUiThread(() -> {
            ytWebView.getSettings().setJavaScriptEnabled(true);
            ytWebView.getSettings().setAppCacheEnabled(true);
            ytWebView.getSettings().setDatabaseEnabled(true);
            ytWebView.getSettings().setDomStorageEnabled(true);
            ytWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            ytWebView.setVisibility(View.VISIBLE);
            ytWebView.setWebViewClient(new MyWebViewClient());

            String idMatch = url.replace("https://www.scorebat.com/api/feed/match/", "");
            linkFinalVideo = "<iframe src='https://www.scorebat.com/embed/g/" + idMatch + "/' frameborder='0' width='560' height='650' allowfullscreen allow='autoplay; fullscreen' style='width:100%;overflow:hidden;' class='_scorebatEmbeddedPlayer_'></iframe><script>(function(d, s, id) {\n" +
                    "    var js, fjs = d.getElementsByTagName(s)[0];\n" +
                    "    if (d.getElementById(id)) return;\n" +
                    "    js = d.createElement(s); js.id = id;\n" +
                    "    js.src = 'https://www.scorebat.com/embed/embed.js?v=arrv';\n" +
                    "    fjs.parentNode.insertBefore(js, fjs);\n" +
                    "  }(document, 'script', 'scorebat-jssdk'));</script>";
            ytWebView.loadDataWithBaseURL("https://youtube.com", linkFinalVideo, "text/html", "base64", null);
        });
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.contains(linkFinalVideo)) {
                // This is my website, so do not override; let my WebView load the page
                return false;
            } else if (Uri.parse(url).getHost().contains("youtub")) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                view.stopLoading();
                startActivity(intent);
                return false;
            }
            view.stopLoading();
            return false;
        }
    }

    protected String doInBackground(String... strings) {
        URL url = null;
        HttpURLConnection urlConnection = null;
        String result = null;
        try {
            url = new URL(strings[0]);
            urlConnection = (HttpURLConnection) url.openConnection(); // Open
            InputStream in = new BufferedInputStream(urlConnection.getInputStream()); // Stream
            result = readStream(in); // Read stream
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return result; // returns the result
    }

    private String readStream(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(is), 1000);
        for (String line = r.readLine(); line != null; line = r.readLine()) {
            sb.append(line);
        }
        is.close();
        return sb.toString();
    }
}
