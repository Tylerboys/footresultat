package com.tyler.footscore.ConfrontationPackage;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tyler.footscore.R;

import java.util.Vector;

public class ConfrontationResAdapter extends BaseAdapter {

    Vector<String> listEvent = new Vector<>();
    private Context context_;
    private LayoutInflater inflater;

    public ConfrontationResAdapter(Context context) {
        context_ = context;
        this.inflater = LayoutInflater.from(context);
    }

    //fonction qui ajoute chaque url des images dans une liste
    public void dd(String event) {
        listEvent.add(event);
        Log.i("TYLER", "Adding event : " + event);
    }

    @Override
    public int getCount() {
        return listEvent.size();
    }

    @Override
    public Object getItem(int position) {
        return listEvent.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = inflater.inflate(R.layout.adapter_item_confrontation_res, null);

        TextView eventView1 = convertView.findViewById(R.id.info_resume_team1);
        TextView eventView2 = convertView.findViewById(R.id.info_resume_team2);

        String ev = listEvent.get(position);
        String typeEvent[] = ev.split(": ");
        ev = typeEvent[1]; //texte de l'evenement avec le nom du joueur et la minute
        String side = ev.substring(ev.length() - 1); // equal to 1 or 2 , précise si l'event est sur l'equipe 1 ou 2


        //Pas mal de condition ci-dessous pour vérifier quel est le type d'évènement qu'on recoit et ainsi afficher
        //l'image View qui correspond, on vérifie aussi quel est l'équipe concerné pour afficher l'imageView de droite
        // ou de gauche dans le layout

        if (typeEvent[0].contentEquals("but ") || typeEvent[0].contentEquals("but (penalty) ") || typeEvent[0].contentEquals("but (csc) ")) {
            if (side.contentEquals("1")) {
                ImageView ivBut = convertView.findViewById(R.id.ballon1);
                ivBut.setVisibility(View.VISIBLE);
            } else if (side.contentEquals("2")) {
                ImageView ivBut = convertView.findViewById(R.id.ballon2);
                ivBut.setVisibility(View.VISIBLE);
            }
        } else if (typeEvent[0].contentEquals("carton jaune ")) {
            if (side.contentEquals("1")) {
                ImageView ivYellowCard = convertView.findViewById(R.id.yellow_card1);
                ivYellowCard.setVisibility(View.VISIBLE);
            } else if (side.contentEquals("2")) {
                ImageView ivYellowCard = convertView.findViewById(R.id.yellow_card2);
                ivYellowCard.setVisibility(View.VISIBLE);
            }
        } else if (typeEvent[0].contentEquals("double jaune (rouge) ")) {
            if (side.contentEquals("1")) {
                ImageView ivYellowToRedCard = convertView.findViewById(R.id.yToRed_card1);
                ivYellowToRedCard.setVisibility(View.VISIBLE);
            } else if (side.contentEquals("2")) {
                ImageView ivYellowToRedCard = convertView.findViewById(R.id.yToRed_card2);
                ivYellowToRedCard.setVisibility(View.VISIBLE);
            }
        } else if (typeEvent[0].contentEquals("carton rouge ")) {
            if (side.contentEquals("1")) {
                ImageView ivRedCard = convertView.findViewById(R.id.red_card1);
                ivRedCard.setVisibility(View.VISIBLE);
            } else if (side.contentEquals("2")) {
                ImageView ivRedCard = convertView.findViewById(R.id.red_card2);
                ivRedCard.setVisibility(View.VISIBLE);
            }
        }

        //affichage du texte de l'évènement à droite ou à gauche selon l'équipe concerné (1 ou 2)
        if (side.contentEquals("1")) {
            eventView1.setText(ev.substring(0, ev.length() - 1));
        } else if (side.contentEquals("2")) {
            eventView2.setText(ev.substring(0, ev.length() - 1));
        }
        return convertView;
    }

}
