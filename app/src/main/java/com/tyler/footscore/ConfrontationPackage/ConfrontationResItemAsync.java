package com.tyler.footscore.ConfrontationPackage;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.tyler.footscore.R;
import com.tyler.footscore.ResultatPackage.ResultatItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;

public class ConfrontationResItemAsync extends AsyncTask<String, Void, JSONObject> {

    private AppCompatActivity myActivity;
    private Context context;
    private ConfrontationResAdapter myadapter;

    public ConfrontationResItemAsync(AppCompatActivity myActivity, Context context, ConfrontationResAdapter myadapter) {
        this.myActivity = myActivity;
        this.context = context;
        this.myadapter = myadapter;
    }

    @Override
    protected JSONObject doInBackground(String... strings) {
        URL url = null;
        HttpURLConnection urlConnection = null;
        String result = null;
        try {
            url = new URL(strings[0]);
            urlConnection = (HttpURLConnection) url.openConnection(); // Open
            InputStream in = new BufferedInputStream(urlConnection.getInputStream()); // Stream
            result = readStream(in); // Read stream
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }

        JSONObject json = null;
        try {
            json = new JSONObject(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return json; // returns the result
    }

    private String readStream(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(is), 1000);
        for (String line = r.readLine(); line != null; line = r.readLine()) {
            sb.append(line);
        }
        is.close();
        return sb.toString();
    }

    public void onPostExecute(JSONObject s) {
        ResultatItem resultatItem;
        try {
            s = s.getJSONObject("response");
            long time = Integer.parseInt(s.getString("dt"));

            // Create a calendar object that will convert the date and time value in milliseconds to date.
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(time * 1000);
            int month = calendar.get(Calendar.MONTH) + 1;
            String date = calendar.get(Calendar.DAY_OF_MONTH) + "/" + month;
            String nameTeam1 = s.getString("s1");
            String nameTeam2 = s.getString("s2");
            String scoreTeam1 = s.getString("sc1");
            String scoreTeam2 = s.getString("sc2");
            Log.i("Tyler", nameTeam1 + scoreTeam1 + scoreTeam2 + nameTeam2);
            resultatItem = new ResultatItem();
            resultatItem.setNameTeam1(nameTeam1);
            resultatItem.setNameTeam2(nameTeam2);
            resultatItem.setScoreTeam1(scoreTeam1);
            resultatItem.setScoreTeam2(scoreTeam2);
            resultatItem.setDateGame(date);
            changeImage(resultatItem);

            JSONArray items = s.getJSONArray("ev");
            for (int i = 0; i < items.length(); i++) {
                JSONObject flickr_entry = items.getJSONObject(i);
                String typeEvent = flickr_entry.getString("t");
                String event = "";
                if (typeEvent.contentEquals("goal")) {
                    event = "but : " + flickr_entry.getString("n") + " (" + flickr_entry.getString("m") + "min)" + flickr_entry.getString("s");
                } else if (typeEvent.contentEquals("yc")) {
                    event = "carton jaune : " + flickr_entry.getString("n") + " (" + flickr_entry.getString("m") + "min)" + flickr_entry.getString("s");
                } else if (typeEvent.contentEquals("2yc")) {
                    event = "double jaune (rouge) : " + flickr_entry.getString("n") + " (" + flickr_entry.getString("m") + "min)" + flickr_entry.getString("s");
                } else if (typeEvent.contentEquals("rc")) {
                    event = "carton rouge : " + flickr_entry.getString("n") + " (" + flickr_entry.getString("m") + "min)" + flickr_entry.getString("s");
                } else if (typeEvent.contentEquals("pen")) {
                    event = "but (penalty) : (penalty)" + flickr_entry.getString("n") + " (" + flickr_entry.getString("m") + "min)" + flickr_entry.getString("s");
                } else if (typeEvent.contentEquals("og")) {
                    event = "but (csc) : (csc)" + flickr_entry.getString("n") + " (" + flickr_entry.getString("m") + "min)" + flickr_entry.getString("s");
                } else if (typeEvent.contentEquals("m.pen")) {
                    event = "penalty rate : penalty rate " + flickr_entry.getString("n") + " (" + flickr_entry.getString("m") + "min)" + flickr_entry.getString("s");
                }
                myadapter.dd(event);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //Fonction qui va changer l'image apparaissant dans le layout et pour accéder aux modification on
    // utilise un runOnUiThread qui va faire éxécuter le code dans le thread principal
    public void changeImage(ResultatItem resultatItem) {
        final ResultatItem resultatItemFinal = resultatItem;
        TextView sc = myActivity.findViewById((R.id.score));
        TextView team1View = myActivity.findViewById((R.id.team1_name));
        TextView team2View = myActivity.findViewById((R.id.team2_name));
        TextView dateOfmatch = myActivity.findViewById((R.id.journee));

        myActivity.runOnUiThread(() -> {
            sc.setText(resultatItem.getScoreTeam1() + "-" + resultatItem.getScoreTeam2());
            String team1 = resultatItem.getNameTeam1();
            String team2 = resultatItem.getNameTeam2();
            if (team1.length() > 16) {
                team1 = team1.substring(0, 15);
            }
            if (team2.length() > 16) {
                team2 = team2.substring(0, 15);
            }

            team1View.setText(team1);
            team2View.setText(team2);
            dateOfmatch.setText("Match du " + resultatItem.getDateGame());
        });
    }

}

