package com.tyler.footscore.ResultatPackage;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.tyler.footscore.CalendrierPackage.CalendrierActivity;
import com.tyler.footscore.MainActivity;
import com.tyler.footscore.R;
import com.tyler.footscore.RankingPackage.RankingActivity;

import org.json.JSONObject;

public class ResultatActivity extends AppCompatActivity {

    private String url = "";
    private Button goToClassement;
    private Button goToCalendrier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.resultat_layout);

        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        if (b != null) {
            url = (String) b.get("keyUrl");
        }

        this.goToClassement = findViewById(R.id.classementButton);
        this.goToCalendrier = findViewById(R.id.calendrierButton);

        ResultatItemAsync res = new ResultatItemAsync(ResultatActivity.this, this);
        Thread thread = new Thread() {
            public void run() {

                String[] tab = new String[1];
                tab[0] = url;
                JSONObject JsObjUrl = res.doInBackground(tab);
                res.onPostExecute(JsObjUrl);
            }
        };
        thread.start();

        goToClassement.setOnClickListener(v -> {
            Intent intent1 = new Intent(v.getContext(), RankingActivity.class);
            intent1.putExtra("keyUrl", url);
            startActivity(intent1);
        });

        goToCalendrier.setOnClickListener(v -> {
            Intent intent12 = new Intent(v.getContext(), CalendrierActivity.class);
            intent12.putExtra("keyUrl", url);
            startActivity(intent12);
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
