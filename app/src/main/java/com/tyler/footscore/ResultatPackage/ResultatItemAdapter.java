package com.tyler.footscore.ResultatPackage;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.tyler.footscore.ConfrontationPackage.ConfrontationResActivity;
import com.tyler.footscore.R;

import java.util.List;

import static java.lang.Integer.parseInt;

public class ResultatItemAdapter extends BaseAdapter {

    private Context context;
    private List<ResultatItem> ResultatItemList;
    private LayoutInflater inflater;

    public ResultatItemAdapter(Context context, List<ResultatItem> ResultatItemList) {
        this.context = context;
        this.ResultatItemList = ResultatItemList;
        this.inflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return ResultatItemList.size();
    }

    @Override
    public ResultatItem getItem(int position) {
        return ResultatItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.adapter_item_resultat, null);

        ResultatItem previousItem = null;
        //get information about item
        if(position!=0){
            previousItem = getItem(position-1);
        }
        ResultatItem currentItem = getItem(position);

        String idgame = currentItem.getIdGame();

        String team1 = currentItem.getNameTeam1();

        String team2 = currentItem.getNameTeam2();

        String scoreTeam1 = currentItem.getScoreTeam1();
        String scoreTeam2 = currentItem.getScoreTeam2();
        String dateGame = currentItem.getDateGame();
        String stateOfTheGame = currentItem.getStateOfTheGame();

        //get item name view
        TextView team1NameView = convertView.findViewById(R.id.team1_name);
        team1NameView.setText(team1);

        TextView team2NameView = convertView.findViewById(R.id.team2_name);
        team2NameView.setText(team2);

        TextView score1View = convertView.findViewById(R.id.score1);
        score1View.setText(scoreTeam1);

        TextView score2View = convertView.findViewById(R.id.score2);
        score2View.setText(scoreTeam2);


        //Affiche une icon bleu si un match est en cours et  lance un toast si on clique sur l'icone pour dire que le match est en cours
        if (!stateOfTheGame.contentEquals("FT") && !stateOfTheGame.contentEquals("-") && !stateOfTheGame.contentEquals("AET") && !stateOfTheGame.contentEquals("Pen")) {
            ImageView liveIconView = convertView.findViewById(R.id.live_icon);
            liveIconView.setVisibility(View.VISIBLE);

            liveIconView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast toast = Toast.makeText(context, "match en cours" , Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER,0,0);
                    toast.show();

                }
            });

        }

        TextView journeeView = convertView.findViewById(R.id.journee);
        TextView teamFavorisNameView = convertView.findViewById(R.id.teamFavoris);

        //Condition if qui va permettre dans la liste d'afficher uniquement 1 fois 1 même date sans
        //se répeter. On verifie si le match précédent possédait la même date que la vue actuelle
        // et dans ce cas on a donc pas besoin de réafficher la date du match.
        if(previousItem == null) {
            journeeView.setText("Matchs du " + dateGame);
            if (context.toString().contains("FavoritesRes")){
                teamFavorisNameView.setText(currentItem.getFavTeam());
                teamFavorisNameView.setVisibility(View.VISIBLE);
            }
        }
        else{
            if(previousItem.getDateGame().contentEquals(currentItem.getDateGame())) {
                journeeView.setVisibility(View.GONE);
            }
            else{
                journeeView.setText("Matchs du " + dateGame);
            }

            if(context.toString().contains("FavoritesRes")) {
                if (!previousItem.getFavTeam().contentEquals(currentItem.getFavTeam())) {
                    teamFavorisNameView.setText(currentItem.getFavTeam());
                    teamFavorisNameView.setVisibility(View.VISIBLE);
                }
            }
        }


        if(parseInt(scoreTeam1)>parseInt(scoreTeam2)){
            team1NameView.setTypeface(null, Typeface.BOLD);
        }
        else if(parseInt(scoreTeam1)<parseInt(scoreTeam2)){
            team2NameView.setTypeface(null, Typeface.BOLD);
        }

        //onclickListener sur les matchs pour ensuite afficher le matchs avec ces détails
        //On redirige sur la prochaine activité avec l'url dont on a besoin avec l'id correspondan au
        //match, cette id sera l'élément le plus important pour que l'activité suivante affiche les bonnes informations
        convertView.setOnClickListener(v -> {
            String url = "https://www.scorebat.com/api/feed/match/"+ idgame ;
            Intent intent = new Intent(v.getContext(), ConfrontationResActivity.class);
            intent.putExtra("keyUrl", url);
            context.startActivity(intent);
        });

        return convertView;
    }
}
