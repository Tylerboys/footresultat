package com.tyler.footscore.ResultatPackage;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.tyler.footscore.CalendrierPackage.FavoritesCalendrierActivity;
import com.tyler.footscore.MainActivity;
import com.tyler.footscore.R;
import com.tyler.footscore.RankingPackage.RankingItemAdapter;

import org.json.JSONObject;

//Activité corresspondant aux résultats dans les équipes favorite
public class FavoritesResultatActivity extends AppCompatActivity {

    private Button goToFavCalendrier;
    private String FavTeamsStr = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favorites_res_layout);

        loadData();

        this.goToFavCalendrier = findViewById(R.id.calendrierButton);

        //verifie si il existe des équipes enregistrer dans favoris
        if (FavTeamsStr.length() != 0) {

            //On récupère le nom de chaque dans un tableau, en enlevant le séparateur placer lors de leur enregistrement
            String[] teamTab = FavTeamsStr.substring(1).split(":");


            FavResultatItemAsync res = new FavResultatItemAsync(FavoritesResultatActivity.this, this, teamTab.length);
            String[] tab = new String[teamTab.length];
            Thread thread = new Thread() {
                public void run() {
                    JSONObject JsObjUrl;

                    //Boucle for pour travailler sur toutes les équipes favorites et afficher leurs résultats les uns après les autres
                    for (int i = 0; i < teamTab.length; i++) {
                        if (teamTab[i].contains("-")) {
                            teamTab[i] = teamTab[i].replace("-", "");
                        } else if (teamTab[i].contains(" ")) {
                            teamTab[i] = teamTab[i].replace(" ", "-");
                        }
                        tab[i] = "https://www.scorebat.com/api/team/" + teamTab[i];
                        JsObjUrl = res.doInBackground(tab[i]);
                        res.onPostExecute(JsObjUrl);

                    }

                }
            };
            thread.start();

        }

        goToFavCalendrier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), FavoritesCalendrierActivity.class);
                startActivity(intent);
            }
        });

    }

    //Recupere le nom des équipes favorites enregister avec Preference
    private void loadData() {
        SharedPreferences sharedPreferences = getSharedPreferences(RankingItemAdapter.SHAREDPREFS, MODE_PRIVATE);
        FavTeamsStr = sharedPreferences.getString("TEXT", "");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
