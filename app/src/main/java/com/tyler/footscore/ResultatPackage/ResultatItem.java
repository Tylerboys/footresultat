package com.tyler.footscore.ResultatPackage;

//Classe contenant toutes les données récupérer dans le json utile pour l'adapter qui permettra d'afficher les listView
public class ResultatItem{
    private String nameTeam1;
    private String nameTeam2;
    private String scoreTeam1;
    private String scoreTeam2;
    private String dateGame;
    private String idGame;
    private String stateOfTheGame;
    private String favTeam;

    //constructor
    public ResultatItem() { }


    //methods
    public String getNameTeam1() {
        return nameTeam1;
    }

    public void setNameTeam1(String nameTeam1) {
        this.nameTeam1 = nameTeam1;
    }

    public String getNameTeam2() {
        return nameTeam2;
    }

    public void setNameTeam2(String nameTeam2) {
        this.nameTeam2 = nameTeam2;
    }

    public String getScoreTeam1() {
        return scoreTeam1;
    }

    public void setScoreTeam1(String scoreTeam1) {
        this.scoreTeam1 = scoreTeam1;
    }

    public String getScoreTeam2() {
        return scoreTeam2;
    }

    public void setScoreTeam2(String scoreTeam2) {
        this.scoreTeam2 = scoreTeam2;
    }

    public String getDateGame() {
        return dateGame;
    }

    public void setDateGame(String dateGame) {
        this.dateGame = dateGame;
    }

    public String getIdGame() { return idGame; }

    public void setIdGame(String idGame) { this.idGame = idGame; }

    public String getStateOfTheGame() {
        return stateOfTheGame;
    }

    public void setStateOfTheGame(String stateOfTheGame) {
        this.stateOfTheGame = stateOfTheGame;
    }

    public String getFavTeam() { return favTeam; }

    public void setFavTeam(String favTeam) { this.favTeam = favTeam; }
}
