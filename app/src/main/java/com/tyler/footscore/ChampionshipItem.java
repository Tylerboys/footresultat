package com.tyler.footscore;

//Classe contenant toutes les données récupérer dans le json utile pour l'adapter qui permettra d'afficher les listView
public class ChampionshipItem {

    private String name;
    private String mnemonic;

    //constructor
    public ChampionshipItem(String name, String mnemonic) {
        this.name = name;
        this.mnemonic = mnemonic;
    }

    //methods
    public String getName() {
        return name;
    }

    public String getMnemonic() {
        return mnemonic;
    }

}
