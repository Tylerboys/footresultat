package com.tyler.footscore.CalendrierPackage;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.tyler.footscore.MainActivity;
import com.tyler.footscore.R;
import com.tyler.footscore.RankingPackage.RankingActivity;
import com.tyler.footscore.ResultatPackage.ResultatActivity;

import org.json.JSONObject;

public class CalendrierActivity extends AppCompatActivity {

    private String url = "";
    private Button goToClassement;
    private Button goToResultat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendrier_layout);

        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        if (b != null) {
            url = (String) b.get("keyUrl");
        }

        this.goToClassement = (Button) findViewById(R.id.classementButton);
        this.goToResultat = (Button) findViewById(R.id.resultatButton);

        CalendrierItemAsync cal = new CalendrierItemAsync(CalendrierActivity.this, this);
        Thread thread = new Thread() {
            public void run() {

                String[] tab = new String[1];
                tab[0] = url;
                JSONObject JsObjUrl = cal.doInBackground(tab);
                cal.onPostExecute(JsObjUrl);
            }
        };
        thread.start();

        goToClassement.setOnClickListener(v -> {
            Intent intent1 = new Intent(v.getContext(), RankingActivity.class);
            intent1.putExtra("keyUrl", url);
            startActivity(intent1);
        });

        goToResultat.setOnClickListener(v -> {
            Intent intent12 = new Intent(v.getContext(), ResultatActivity.class);
            intent12.putExtra("keyUrl", url);
            startActivity(intent12);
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
