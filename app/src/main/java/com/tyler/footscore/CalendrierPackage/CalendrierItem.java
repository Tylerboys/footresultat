package com.tyler.footscore.CalendrierPackage;

//Classe contenant toutes les données récupérer dans le json utile pour l'adapter qui permettra d'afficher les listView
public class CalendrierItem {
    private String nameTeam1;
    private String nameTeam2;
    private String hourOfMatch;
    private String dateGame;

    //constructor
    public CalendrierItem() {
    }


    //methods
    public String getNameTeam1() {
        return nameTeam1;
    }

    public void setNameTeam1(String nameTeam1) {
        this.nameTeam1 = nameTeam1;
    }

    public String getNameTeam2() {
        return nameTeam2;
    }

    public void setNameTeam2(String nameTeam2) {
        this.nameTeam2 = nameTeam2;
    }

    public String getHourOfMatch() {
        return hourOfMatch;
    }

    public void setHourOfMatch(String hourOfMatch) {
        this.hourOfMatch = hourOfMatch;
    }

    public String getDateGame() {
        return dateGame;
    }

    public void setDateGame(String dateGame) {
        this.dateGame = dateGame;
    }
}
