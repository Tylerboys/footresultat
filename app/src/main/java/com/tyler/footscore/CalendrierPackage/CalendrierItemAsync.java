package com.tyler.footscore.CalendrierPackage;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.tyler.footscore.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CalendrierItemAsync extends AsyncTask<String, Void, JSONObject> {

    private AppCompatActivity myActivity;
    private Context context;

    public CalendrierItemAsync(AppCompatActivity myActivity, Context context) {
        this.myActivity = myActivity;
        this.context = context;
    }

    @Override
    protected JSONObject doInBackground(String... strings) {
        URL url = null;
        HttpURLConnection urlConnection = null;
        String result = null;
        try {
            url = new URL(strings[0]);
            urlConnection = (HttpURLConnection) url.openConnection(); // Open
            InputStream in = new BufferedInputStream(urlConnection.getInputStream()); // Stream
            result = readStream(in); // Read stream
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }

        JSONObject json = null;
        try {
            json = new JSONObject(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return json; // returns the result
    }

    private String readStream(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(is), 1000);
        for (String line = r.readLine(); line != null; line = r.readLine()) {
            sb.append(line);
        }
        is.close();
        return sb.toString();
    }

    public void onPostExecute(JSONObject s) {
        List<CalendrierItem> CalendrierItemList = new ArrayList<>();
        try {
            s = s.getJSONObject("response");
            JSONArray items = s.getJSONArray("fixtures");
            for (int i = 0; i < items.length(); i++) {
                JSONObject flickr_entry = items.getJSONObject(i);
                long time = Integer.parseInt(flickr_entry.getString("dt"));

                // Create a calendar object that will convert the date and time value in milliseconds to date.
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(time * 1000);
                int month = calendar.get(Calendar.MONTH) + 1;
                String date = calendar.get(Calendar.DAY_OF_MONTH) + "/" + month;
                String nameTeam1 = flickr_entry.getString("s1");
                String nameTeam2 = flickr_entry.getString("s2");

                String heure = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));

                String minute = String.valueOf(calendar.get(Calendar.MINUTE));
                if (minute.length() == 1) {
                    minute = 0 + minute;
                }

                String hourOfMatch;
                if (heure.contentEquals("0")) {
                    hourOfMatch = "Reporté";
                } else {
                    hourOfMatch = heure + ":" + minute;
                }
                CalendrierItemList.add(new CalendrierItem());
                CalendrierItemList.get(i).setNameTeam1(nameTeam1);
                CalendrierItemList.get(i).setNameTeam2(nameTeam2);
                CalendrierItemList.get(i).setHourOfMatch(hourOfMatch);
                CalendrierItemList.get(i).setDateGame(date);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        toDoAdapter(CalendrierItemList);
    }

    public void toDoAdapter(List<CalendrierItem> CalendrierItemList) {
        ListView calListView = (ListView) myActivity.findViewById(R.id.calendrier_list_view);
        myActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                calListView.setAdapter(new CalendrierItemAdapter(context, CalendrierItemList));
            }
        });
    }

}
