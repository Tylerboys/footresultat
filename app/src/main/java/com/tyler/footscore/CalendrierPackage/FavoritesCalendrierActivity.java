package com.tyler.footscore.CalendrierPackage;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.tyler.footscore.ResultatPackage.FavoritesResultatActivity;
import com.tyler.footscore.MainActivity;
import com.tyler.footscore.R;
import com.tyler.footscore.RankingPackage.RankingItemAdapter;

import org.json.JSONObject;

//Activité corresspondant au calendrier dans les équipes favorite
public class FavoritesCalendrierActivity extends AppCompatActivity {

    private Button goToResultat;
    private String FavTeamsStr="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favorites_team_layout);

        loadData();

        this.goToResultat = (Button) findViewById(R.id.resultatButton);

        if(FavTeamsStr.length()!=0){
            String[] teamTab = FavTeamsStr.substring(1).split(":");


        FavCalendrierItemAsync cal = new FavCalendrierItemAsync(FavoritesCalendrierActivity.this, this, teamTab.length);
            String[] tab = new String[teamTab.length];
            Thread thread = new Thread() {
                public void run() {
                    JSONObject JsObjUrl;

            for(int i =0; i< teamTab.length;i++){
                if(teamTab[i].contains("-")){
                    teamTab[i] = teamTab[i].replace("-","");
                }
                else if(teamTab[i].contains(" ")){
                    teamTab[i] = teamTab[i].replace(" ","-");
                }
                tab[i] = "https://www.scorebat.com/api/team/"+ teamTab[i];
                JsObjUrl = cal.doInBackground(tab[i]);
                cal.onPostExecute(JsObjUrl);

            }

            }
        };
        thread.start();

        }

        goToResultat.setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), FavoritesResultatActivity.class);
            startActivity(intent);
        });

    }

    //Recupere le nom des équipes favorites enregister avec Preference
    private void loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences(RankingItemAdapter.SHAREDPREFS, MODE_PRIVATE);
        FavTeamsStr = sharedPreferences.getString("TEXT", "");
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
