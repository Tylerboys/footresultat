package com.tyler.footscore.CalendrierPackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tyler.footscore.R;

import java.util.List;

public class CalendrierItemAdapter extends BaseAdapter {

    private Context context;
    private List<CalendrierItem> CalendrierItemList;
    private LayoutInflater inflater;

    public CalendrierItemAdapter(Context context, List<CalendrierItem> CalendrierItemList) {
        this.context = context;
        this.CalendrierItemList = CalendrierItemList;
        this.inflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return CalendrierItemList.size();
    }

    @Override
    public CalendrierItem getItem(int position) {
        return CalendrierItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.adapter_item_calendrier, null);

        CalendrierItem previousItem = null;

        //obtenir les informations sur l'élément précédent pour faire des comparaison par la suite et ne pas répéter
        //l'affichage d'un élément inutilement
        if (position != 0) {
            previousItem = getItem(position - 1);
        }
        CalendrierItem currentItem = getItem(position);
        String team1 = currentItem.getNameTeam1();
        if (team1.length() > 20) {
            team1 = team1.substring(0, 20);
        }
        String team2 = currentItem.getNameTeam2();
        if (team2.length() > 20) {
            team2 = team2.substring(0, 20);
        }
        String hour = currentItem.getHourOfMatch();
        String dateGame = currentItem.getDateGame();

        //get item name view
        TextView team1NameView = convertView.findViewById(R.id.team1_name);
        team1NameView.setText(team1);

        TextView team2NameView = convertView.findViewById(R.id.team2_name);
        team2NameView.setText(team2);

        TextView hourOfmatch = convertView.findViewById(R.id.hourOfMatch);
        hourOfmatch.setText(hour);

        TextView journee = convertView.findViewById(R.id.journee);

        //Condition if qui va permettre dans la liste d'afficher uniquement 1 fois 1 même date sans
        //se répeter. On verifie si le match précédent possédait la même date que la vue actuelle
        // et dans ce cas on a donc pas besoin de réafficher la date du match.
        if (previousItem == null) {
            journee.setText("Matchs du " + dateGame);
        } else {
            if (previousItem.getDateGame().contentEquals(currentItem.getDateGame())) {
                convertView.findViewById(R.id.journee).setVisibility(View.GONE);
            } else {
                journee.setText("Matchs du " + dateGame);
            }
        }
        return convertView;
    }
}
