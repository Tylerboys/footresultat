package com.tyler.footscore.RankingPackage;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;


import com.tyler.footscore.R;

import java.util.List;


public class RankingItemAdapter extends BaseAdapter {

    private Context context;
    private List<RankingItem> RankingItemList;
    private LayoutInflater inflater;
    private String FavTeamsStr = "";

    public static final String SHAREDPREFS = "sharedPrefs";

    public RankingItemAdapter(Context context, List<RankingItem> RankingItemList) {
        this.context = context;
        this.RankingItemList = RankingItemList;
        this.inflater = LayoutInflater.from(context);
        loadData(); //récupère noms des équipes favorites dans le String FavTeamsStr
    }

    @Override
    public int getCount() {
        return RankingItemList.size();
    }

    @Override
    public RankingItem getItem(int position) {
        return RankingItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.adapter_item_ranking, null);
        //get information about item
        CheckBox favTeamCheckBox = convertView.findViewById(R.id.favTeamCheckBox);

        RankingItem currentItem = getItem(position);
        String itemName = currentItem.getName();
        String teamPoint = currentItem.getPoint();
        String ranking = currentItem.getRank();
        String gamePlayed = currentItem.getGamePld();

        //get item name view
        TextView teamNameView = convertView.findViewById(R.id.team_name);
        if (itemName.length() > 20) {
            itemName = itemName.substring(0, 20);
        }
        teamNameView.setText(itemName);

        //get item point view
        TextView pointView = convertView.findViewById(R.id.team_points);
        pointView.setText(teamPoint);

        //get item point view
        TextView rankView = convertView.findViewById(R.id.ranking);
        rankView.setText(ranking);

        TextView gPlayed = convertView.findViewById(R.id.NbrGamePlyd);
        gPlayed.setText(gamePlayed);

        //Recoche les équipes favorites si leurs nom apparait dans le String FavTeamsStr
        // qui aura load les équipes favorites au lancement à la création de cette classe
        if (FavTeamsStr.contains(currentItem.getName())) {
            favTeamCheckBox.setChecked(true);
        }

        favTeamCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CompoundButton) view).isChecked()) {
                    Toast.makeText(context, "Equipe ajouter aux favoris", Toast.LENGTH_SHORT).show();
                    FavTeamsStr += ":" + currentItem.getName();
                } else {
                    Toast.makeText(context, "Equipe retirer des favoris", Toast.LENGTH_SHORT).show();
                    String removeFav = ":" + currentItem.getName();
                    FavTeamsStr = FavTeamsStr.replace(removeFav, "");
                }
                saveFavoris();
            }

            //Fonction qui permet d'enregister le nom des équipes ajouter aux favoris
            private void saveFavoris() {
                SharedPreferences sharedPreferences = context.getSharedPreferences(SHAREDPREFS, context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();

                editor.putString("TEXT", FavTeamsStr);
                editor.apply();
            }
        });

        return convertView;
    }

    //Fonction qui permet de récupérer le nom des équipes enregistrer dans les favoris dès lancement de la view
    private void loadData() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHAREDPREFS, context.MODE_PRIVATE);
        FavTeamsStr = sharedPreferences.getString("TEXT", "");
    }
}
