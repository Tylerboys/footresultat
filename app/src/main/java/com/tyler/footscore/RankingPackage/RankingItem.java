package com.tyler.footscore.RankingPackage;

//Classe contenant toutes les données récupérer dans le json utile pour l'adapter qui permettra d'afficher les listView
public class RankingItem {
    private String name;
    private String rank;
    private String point;


    private String gamePld;//game played

    //constructor
    public RankingItem() {
    }


    //methods
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getGamePld() {
        return gamePld;
    }

    public void setGamePld(String gamePld) {
        this.gamePld = gamePld;
    }


}
