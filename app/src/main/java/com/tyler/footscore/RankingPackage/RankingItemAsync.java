package com.tyler.footscore.RankingPackage;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.tyler.footscore.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class RankingItemAsync extends AsyncTask<String, Void, JSONObject> {

    private AppCompatActivity myActivity;
    private Context context;

    public RankingItemAsync(AppCompatActivity myActivity, Context context) {
        this.myActivity = myActivity;
        this.context = context;
    }

    @Override
    protected JSONObject doInBackground(String... strings) {
        URL url = null;
        HttpURLConnection urlConnection = null;
        String result = null;
        try {
            url = new URL(strings[0]);
            urlConnection = (HttpURLConnection) url.openConnection(); // Open
            InputStream in = new BufferedInputStream(urlConnection.getInputStream()); // Stream
            result = readStream(in); // Read stream
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }

        JSONObject json = null;
        try {
            json = new JSONObject(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return json; // returns the result
    }

    private String readStream(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(is), 1000);
        for (String line = r.readLine(); line != null; line = r.readLine()) {
            sb.append(line);
        }
        is.close();
        return sb.toString();
    }

    public void onPostExecute(JSONObject s) {
        List<RankingItem> RankingItemList = new ArrayList<>();
        try {
            s = s.getJSONObject("response");
            s = s.getJSONObject("standings");
            JSONArray items = s.getJSONArray("rows");
            for (int i = 0; i < items.length(); i++) {
                JSONObject flickr_entry = items.getJSONObject(i);
                String nameTeam = flickr_entry.getString("team");
                String point = flickr_entry.getString("pnt");
                String rank = flickr_entry.getString("row");
                String gamePlayed = flickr_entry.getString("p");
                Log.i("TYLER", i + "nom equipe: " + nameTeam + ",rank :" + rank + " with " + point + " points.");
                RankingItemList.add(new RankingItem());
                RankingItemList.get(i).setName(nameTeam);
                RankingItemList.get(i).setPoint(point);
                RankingItemList.get(i).setRank(rank);
                RankingItemList.get(i).setGamePld(gamePlayed);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        toDoAdapter(RankingItemList);
    }

    public void toDoAdapter(List<RankingItem> RankingItemList) {
        ListView rankListView = myActivity.findViewById(R.id.ranking_list_view);
        myActivity.runOnUiThread(() -> rankListView.setAdapter(new RankingItemAdapter(context, RankingItemList)));
    }

}
