package com.tyler.footscore.RankingPackage;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.tyler.footscore.CalendrierPackage.CalendrierActivity;
import com.tyler.footscore.MainActivity;
import com.tyler.footscore.R;
import com.tyler.footscore.ResultatPackage.ResultatActivity;

import org.json.JSONObject;

public class RankingActivity extends AppCompatActivity {

    private Button goToResultat;
    private Button goToCalendrier;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ranking_layout);


        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        if (b != null) {
            url = (String) b.get("keyUrl");
        }

        this.goToResultat = findViewById(R.id.resultatButton);
        this.goToCalendrier = findViewById(R.id.calendrierButton);

        RankingItemAsync rank = new RankingItemAsync(RankingActivity.this, this);
        Thread thread = new Thread() {
            public void run() {

                String[] tab = new String[1];
                tab[0] = url;
                JSONObject JsObjUrl = rank.doInBackground(tab);
                rank.onPostExecute(JsObjUrl);
            }
        };
        thread.start();


        goToResultat.setOnClickListener(v -> {
            Intent intent1 = new Intent(v.getContext(), ResultatActivity.class);
            intent1.putExtra("keyUrl", url);
            startActivity(intent1);
        });

        goToCalendrier.setOnClickListener(v -> {
            Intent intent12 = new Intent(v.getContext(), CalendrierActivity.class);
            intent12.putExtra("keyUrl", url);
            startActivity(intent12);
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}