package com.tyler.footscore;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class ChampionshipItemAdapter extends BaseAdapter {

    private Context context;
    private List<ChampionshipItem> championshipItemList;
    private LayoutInflater inflater;

    public ChampionshipItemAdapter(Context context, List<ChampionshipItem> championshipItemList) {
        this.context = context;
        this.championshipItemList = championshipItemList;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return championshipItemList.size();
    }

    @Override
    public ChampionshipItem getItem(int position) {
        return championshipItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.adapter_item, null);
        //get information about item
        ChampionshipItem currentItem = getItem(position);
        String itemName = currentItem.getName();
        String mnemonic = currentItem.getMnemonic();

        //get item mnemocic view
        ImageView itemIconView = convertView.findViewById(R.id.item_icon);
        int resId = context.getResources().getIdentifier(mnemonic, "drawable", context.getPackageName());
        itemIconView.setImageResource(resId);

        //get item name view
        TextView itemNameView = convertView.findViewById(R.id.item_name);
        itemNameView.setText(itemName);

        return convertView;
    }
}
