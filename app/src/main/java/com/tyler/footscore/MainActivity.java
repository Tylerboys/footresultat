package com.tyler.footscore;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.tyler.footscore.CalendrierPackage.FavoritesCalendrierActivity;
import com.tyler.footscore.RankingPackage.RankingActivity;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private String urlLeague;
    private RelativeLayout goToFavoris;
    private int backButtonCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        goToFavoris = findViewById(R.id.favoris_button);

        //list of championship
        List<ChampionshipItem> ChampionshipItemList = new ArrayList<>();
        ChampionshipItemList.add(new ChampionshipItem("Ligue 1", "ligue1"));
        ChampionshipItemList.add(new ChampionshipItem("Premier League", "epl"));
        ChampionshipItemList.add(new ChampionshipItem("LaLiga", "laliga"));
        ChampionshipItemList.add(new ChampionshipItem("Bundesliga", "bundesliga"));
        ChampionshipItemList.add(new ChampionshipItem("Serie A", "seriea"));

        //get List view
        ListView chmpListView = findViewById(R.id.championship_list_view);
        chmpListView.setAdapter(new ChampionshipItemAdapter(this, ChampionshipItemList));

        chmpListView.setOnItemClickListener((parent, view, position, id) -> {
            if (position == 0) {
                Intent intent = new Intent(view.getContext(), RankingActivity.class);
                urlLeague = "https://www.scorebat.com/api/competition/3/france-ligue-1/?_=1613523194576&sf=1";
                intent.putExtra("keyUrl", urlLeague);
                startActivity(intent);
            } else if (position == 1) {
                Intent intent = new Intent(view.getContext(), RankingActivity.class);
                urlLeague = "https://www.scorebat.com/api/competition/3/england-premier-league/?_=1613617327685&sf=1";
                intent.putExtra("keyUrl", urlLeague);
                startActivity(intent);
            } else if (position == 2) {
                Intent intent = new Intent(view.getContext(), RankingActivity.class);
                urlLeague = "https://www.scorebat.com/api/competition/3/spain-la-liga/?_=1613617858886&sf=1";
                intent.putExtra("keyUrl", urlLeague);
                startActivity(intent);
            } else if (position == 3) {
                Intent intent = new Intent(view.getContext(), RankingActivity.class);
                urlLeague = "https://www.scorebat.com/api/competition/3/germany-bundesliga/?_=1613617758624&sf=1";
                intent.putExtra("keyUrl", urlLeague);
                startActivity(intent);
            } else if (position == 4) {
                Intent intent = new Intent(view.getContext(), RankingActivity.class);
                urlLeague = "https://www.scorebat.com/api/competition/3/italy-serie-a/?_=1613617244568&sf=1";
                intent.putExtra("keyUrl", urlLeague);
                startActivity(intent);
            }
        });

        goToFavoris.setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), FavoritesCalendrierActivity.class);
            startActivity(intent);
        });
    }

    @Override
    public void onBackPressed() {
        if (backButtonCount >= 1) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            backButtonCount = 0;
            startActivity(intent);
        } else {
            Toast.makeText(this, "Press the back button once again to close the application.", Toast.LENGTH_SHORT).show();
            backButtonCount++;
        }
    }
}